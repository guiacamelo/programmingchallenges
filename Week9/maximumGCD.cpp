
#include <iostream>
#include <cstdio>
#include <vector>
#include <cmath>
#include <algorithm>
#include <cstring>
#include <map>
#include <sstream>
using namespace std;



long gcd(long p,long q)
{
  if (q>p) 
    return (gcd(q,p));
  if (q==0) 
    return (p);
  return (gcd(q,p%q));
}

int main(){
  int N,count;
  long c;
  cin >> N;
  cin.ignore();
  count = N;
  while(count--){
    string line;
    getline(cin, line);
    istringstream iss(line);
    vector<long> numbers;
    while ( iss >> c) {    
   
 
      numbers.push_back(c);

      
    } 
  
    long maxgcd=-100000;
    for(int i=0; i<numbers.size();i++){
      for(int j=i+1; j<numbers.size();j++){
	long currentgcd = 0;
	currentgcd = gcd(numbers[i],numbers[j]);
	if (currentgcd>maxgcd)
	  maxgcd = currentgcd;	
      }
      
    }
    cout<< maxgcd<< endl;
  }
  

  return 0;
}
