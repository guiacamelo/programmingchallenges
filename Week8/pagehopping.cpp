#include <cstdio>
#include <algorithm>
#include <cstring>
using namespace std;
const int SIZE = 105;

int Graph[SIZE][SIZE];
bool bar[SIZE];

int main() {
  int u, v, count = 1;
  
  while (true) {
    
    scanf("%d %d", &u, &v);
    if (u == 0 && v == 0)
      break;

    memset(Graph, 63, sizeof Graph);
    memset(bar, false, sizeof bar);
    
    while (true) {
      bar[u] = true;
      bar[v] = true;
      Graph[u][v] = 1;
      
      scanf("%d %d", &u, &v);
      if (u == 0 && v == 0)
	break;
    }
    
    for (int k = 0; k < SIZE; k++){
      for (int i = 0; i < SIZE; i++){
	for (int j = 0; j < SIZE; j++){
	  Graph[i][j] = min(Graph[i][j],Graph[i][k] + Graph[k][j]);
	}
      }
    }

    double pairsum = 0;
    double possiblepairs = 0;

    for (int i = 0; i < SIZE; i++) {
      for (int j = 0; j < SIZE; j++) {
	if (i == j)
	  continue;
	if (bar[i] && bar[j]) {
	  pairsum += Graph[i][j];
	  possiblepairs++;
	}
      }
    }
    
    printf("Case %d: average length between pages = %.3lf clicks\n",
	   count++, pairsum / possiblepairs);
  }
  
  return 0;
}
