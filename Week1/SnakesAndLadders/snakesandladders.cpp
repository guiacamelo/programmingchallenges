#include<iostream>
#include <algorithm>

using namespace std;

/*
// The input has format

testCases
players snakes dieRolls
snakeMouth snakeTail
snakeMouth snakeTail
snakeMouth snakeTail
dieRoll
dieRoll
dieRoll


*/
const int DEBUG=0;

const int MAX_PLAYERS=1000000;
const int MAX_DIEROLLS=1000000;

int main()
{
  unsigned int testCases,snakes,dieRolls;  
  unsigned int snakeMouth,snakeTail;
  unsigned int dieRoll;
  bool end = false;
  
  //keep track of plyer number
  
  cin >> testCases;
  int testCaseCount=testCases;
  while(testCaseCount-->0)
  {
    
    unsigned int players;
    cin >> players >> snakes >> dieRolls ;
    if (DEBUG)
    {
      cout << endl;    
      cout<<"Config"<<endl;
      cout <<players << " "<<
	snakes <<" "<<
	dieRolls<<endl;
    }
        
    // counter to read lines of file
    int snakesCount = snakes; 
    
    // counter to read line of file
    int rollCount =dieRolls ; 
    // vector to keep beggining and end of snakes and ladders, 
    int snakesPosition[2*snakes];      
    int snakePositionCount=0;
        
    int playerPosition[players];
    int playerRollCount = 0;
    
    // every player start in position 1
    for (int i =0;i<players;i++)
      {
	playerPosition[i] = 1;	
	
      }
    
    while(snakesCount-- >0)
    {
      
      cin >> snakesPosition[snakePositionCount] >> snakesPosition[snakePositionCount+1];
      /*
      if (snakesPosition[snakePositionCount] > snakesPosition[snakePositionCount+1])
	{
	  snakesPosition[snakePositionCount]=0;
	  snakesPosition[snakePositionCount+1]=0;
	}	*/
      //print read values when DEBUG is on
      if (DEBUG)
	{	 
	  cout<<snakesPosition[snakePositionCount]<< " " << 
	    snakesPosition[snakePositionCount+1]<<endl;
	}
      snakePositionCount+=2;
    }
    rollCount = dieRolls;
    while(rollCount-- >0)
    {
      unsigned int dieValue=0; 

      cin >> dieValue;

      if (!end)
	{
	  if((dieRolls>players)&&(playerRollCount>=players))
	    playerRollCount = 0;
	  
	  /*if (playerRollCount < players)
	  //First round have to add +1 since we do not start from zero
	  playerPosition[playerRollCount] = dieValue+1;	
	  else  	//other rounds
	  */
	  playerPosition[playerRollCount] += dieValue;
	  
	  
	  //check if new position had latter or snake
	  for (int i = 0;i<2*snakes;i+=2)
	    {
	      if (playerPosition[playerRollCount] == snakesPosition[i])
		//found lader going back
		playerPosition[playerRollCount] = snakesPosition[i+1];
	      /*   else if(playerPosition[playerRollCount] == snakesPosition[i+1])
		//found lader going forward
		playerPosition[playerRollCount] = snakesPosition[i];	        
	      */	    
	    }
	  
	  if (playerPosition[playerRollCount]>=100)
	    {
	      end=true;
	      
	      // if player position is highter than 100, it stay in position 100
	      
	      playerPosition[playerRollCount] = 100;
	      
	    }
	  
	  
	  //dEbug
	  if (DEBUG)
	    {	  
	      cout<<playerPosition[playerRollCount]<<endl;	 
	    }
	  
	  
	  
	  
	  playerRollCount++;
	}
    }
    //print players and positions
    for (int i =0;i<players;i++)
    {
      cout<<"Position of player "<<i+1<<" is "<< playerPosition[i]<<"."<<endl;	 
      
    }
    // set end to false for next test case
    end = false;  
  }
   
}
