#include<iostream>
#include <algorithm>

using namespace std;

/*
// The input is N B H W
N - Number of participants
B - Budget
H - Hotels to consider
W - Number of weeks to be chosen from
p - price per persopn
a - number of available beds
*/
const int DEBUG=0;

//const int MAX_BUDGET=500001;
int main()
{
  unsigned int N,B,H, W, p, a;  
  int i,j, countH, countW;
  unsigned int max_per_participant;
  int min_price_found = 500001;
  int price;
  
  max_per_participant = (B / N);
  while(cin >> N >> B>>H >>W)
  {
    countH=H;
    while(countH-- > 0){
      countW=W;
      cin >> p ;
      if(DEBUG)
	cout <<p <<endl;
      while(countW-- > 0)
      {   
	
	cin >> a ;
	if (DEBUG)
	  cout<<"count W "<<countW<<endl;

	//check if there is more available beds(a) then participants(N)
	if (a>=N)
	{
	  price = p*N;
	  if (DEBUG)
	  {
	    cout << endl;
	    cout <<p <<endl;
	    cout <<a <<endl;
	    cout<<price<<endl;
	    cout << endl;
	  }
	  
	  //check if schedule is within the budget
	  if (B>price)
	    // check if this schedule is cheeper than the last
	  {
	    if (DEBUG)
	    {
	      cout <<" Schedule within budget  "<<min_price_found <<endl;
	    }
	    if (price < min_price_found)
	    {
	      min_price_found = price;
	      if (DEBUG)
	      {
		cout << endl;
		cout <<" min price  "<<min_price_found <<endl;
		cout << endl;
	      }
	    }
	  }
	  
	}
      }
    }
    //cout << N <<" " << B << " "<< H << " "<<W << endl;

    if (min_price_found == 500001)
      cout<<"stay home"<<endl;
    else
      cout<<min_price_found<<endl;  
    min_price_found = 500001;

  }


}
