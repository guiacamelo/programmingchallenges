#include<iostream>
#include <algorithm>

using namespace std;

int f(int n, int c){
  //n number to test
  //c size of cile
  if (n==1)
    return c+1;
  if (n%2!=0) //impar
    return f(3*n+1,c+1);
  
  else 
    return f(n/2,c+1);
  
}


int main()
{
  int i,j,swap=-1;
  int max_cycle=0;
  int u ,v ;
  while(cin >> i >> j){
/*    if (i>j)
    {
      swap = j;
      j= i;
      i=swap;
      } */
    u = max(i,j);
    v = min(i,j);
    max_cycle = 0;
    for ( int k=v; k<=u;k++){   
      int temp = f(k,0);
      if (temp>max_cycle)
	max_cycle = temp;
    }
    
    cout << i <<" " << j << " "<< max_cycle << endl;
  }
}
