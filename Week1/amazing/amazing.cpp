#include<iostream>
#include<algorithm>
#include<vector>
using namespace std;

typedef vector< vector<int> > Matrix;

const int DEBUG=1;

int main()
{
  unsigned int rows, cols;  
  unsigned int dieRoll;

  //keep track of plyer number
  cin >> rows >> cols;
  int maze [rows][cols];
  cout << rows<< " " << cols<<endl;
  bool leave = false;
  while(!leave){
    
    if ((rows==0)&&(cols==0)){
      break;
      leave = true;
    }
    string blankSpace;
    getline(cin, blankSpace);
    for(unsigned i=0;i<rows;i++) {     
      string line;       
      getline(cin, line);
      for (unsigned j=0;j<cols;j++)
      {
	if (line[j]=='1')
	{
	  maze[i][j] = -1;
	}else
  	  maze[i][j] = 0;	
      }

     }
    leave=true;
    if(DEBUG){
      for(int i=0;i<rows;i++) {
	for (int j=0;j<cols;j++){
	  
	  cout<<":"<<maze[i][j];
	}
	cout<<endl;
      }
    }
  }
  int current_line=rows-1;
  int current_col=0; 
  int max_line=rows-1;
  int min_line=0;
  int max_col=cols-1;
  int min_col=0;
  leave=false;
  while(!leave){
    
    //try to go South
    
    int try_line = current_line+1;
    int try_col = current_col;
    
    if((try_line<=max_line)&& (maze[try_line][try_col]!=-1)){
      maze[try_line][try_col]++;
      current_line=try_line;
      current_col=try_col;
      cout<<"Going South"<<endl;
      cout<<"l-"<<current_line<<endl;
      cout<<"c-"<<current_col<<endl;
	  
      if(DEBUG){
	for(int i=0;i<rows;i++) {
	  for (int j=0;j<cols;j++){
	    
	    cout<<":"<<maze[i][j];
	  }
	  cout<<endl;
	}
      }
    } else
    {
      //try to go East
      int try_line = current_line;
      int try_col  = current_col++;
      
      if((try_col<=max_col)&& (maze[try_line][try_col]!=-1)){
	maze[try_line][try_col]++;
	current_line=try_line;
	current_col=try_col+1;
	cout<<"Going East"<<endl;
	cout<<"l-"<<current_line<<endl;
	cout<<"c-"<<current_col<<endl;

	
        if(current_col>=max_col){
	  current_col=max_col;
	  current_line--;
	 }
	if(DEBUG){
	  for(int i=0;i<rows;i++) {
	    for (int j=0;j<cols;j++){
	      
	      cout<<":"<<maze[i][j];
	    }
	    cout<<endl;
	  }
	}
      }else
      {
	//try to go North
	int try_line = current_line--;
	int try_col  = current_col;
	
	if((try_col>=min_col)&& (maze[try_line][try_col]!=-1)){
	  maze[try_line][try_col]++;
	  current_line=try_line--;
	  current_col=try_col;
	  cout<<"Going North"<<endl;
	  cout<<"l-"<<current_line<<endl;
	  cout<<"c-"<<current_col<<endl;
	  
	  if(DEBUG){
	    for(int i=0;i<rows;i++) {
	      for (int j=0;j<cols;j++){
		
		cout<<":"<<maze[i][j];
	      }
	      cout<<endl;
	    }
	  }
	} else
	{
	  //try to go West
	  int try_line = current_line;
	  int try_col  = current_col--;
	  
	  if((try_col>=min_col)&& (maze[try_line][try_col]!=-1)){
	    maze[try_line][try_col]++;
	    current_line=try_line;
	    current_col=try_col;
	    cout<<"Going West"<<endl;
	    cout<<"l-"<<current_line<<endl;
	    cout<<"c-"<<current_col<<endl;
	  
	    if(DEBUG){
	      for(int i=0;i<rows;i++) {
		for (int j=0;j<cols;j++){
		  
		  cout<<":"<<maze[i][j];
		}
		cout<<endl;
	      }
	    }
	  }
	}
	//back to initial position
	if ((current_line==rows-1)&&(current_col==0)){
	  
	  leave =true;
	  break;
	}
      }
    }
  }
  
    
  // if(DEBUG){
  //   for(int i=0;i<rows;i++) {
  //     for (int j=0;j<cols;j++){
	
  // 	cout<<":"<<maze[i][j];
  //     }
  //     cout<<endl;
  //   }
  // }
}


