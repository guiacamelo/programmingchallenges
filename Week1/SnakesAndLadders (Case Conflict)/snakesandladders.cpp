#include<iostream>
#include <algorithm>

using namespace std;

/*
// The input has format

testCases
players snakes dieRolls
snakeMouth snakeTail
snakeMouth snakeTail
snakeMouth snakeTail
dieRoll
dieRoll
dieRoll


*/
const int DEBUG=1;

const int MAX_PLAYERS=1000000;
const int MAX_DIEROLLS=1000000;

int main()
{
  unsigned int testCases,players,snakes,dieRolls;  
  unsigned int snakeMouth,snakeTail;
  unsigned int dieRoll;
  bool end = false;
  //keep track of plyer number
  
  cin >> testCases;
  int testCaseCount=testCases;
  while(testCaseCount-->0)
  {
    
    
    cin >> players >> snakes >> dieRolls ;
    if (DEBUG)
    {
      cout << endl;      
      cout <<players << " "<<
	snakes <<" "<<
	dieRolls<<endl;
    }
        
    // counter to read lines of file
    int snakesCount = snakes; 
    
    // counter to read line of file
    int rollCount =dieRolls ; 
    
    // vector to keep beggining and end of snakes and ladders, 
    int snakesPosition[2*snakes];      
    int snakePositionCount=0;
        
    int playerPosition[dieRolls];
    int playerRollCount = 0;
    bool first = true;
    while(snakesCount-- >0)
    {
      cin >> snakesPosition[snakePositionCount] >> snakesPosition[snakePositionCount+1];
      
      //print read values when DEBUG is on
      if (DEBUG)
      {	 
	cout<<snakesPosition[snakePositionCount]<< " " << 
	  snakesPosition[snakePositionCount+1]<<endl;
      }
      snakePositionCount+=2;
    }
    
    while(rollCount-- >0)
    {
      if (end)
	break;
      unsigned int dieValue=0; 
      cin >> dieValue;
       
      //dEbug
      if (DEBUG)
      {	  
	cout<<dieValue<< " For player "<<playerRollCount <<endl;	 
      }
      
      if (first){	
	//First round have to add +1 since we do not start from zero
	first=false;
	playerPosition[playerRollCount] = dieValue+1;
      }
      else  	//other rounds
	playerPosition[playerRollCount] += dieValue;
      
      //check if new position had latter or snake
      for (int i = 0;i<2*snakes;i+=2)
      {
	if (playerPosition[playerRollCount] == snakesPosition[i])
	  //found lader going back
	  playerPosition[playerRollCount] = snakesPosition[i+1];
	else if(playerPosition[playerRollCount] == snakesPosition[i+1])
	  //found lader going forward
	  playerPosition[playerRollCount] = snakesPosition[i];	        
      }
      
      if (playerPosition[playerRollCount]>=100)
      {
	end=true;	
      }
      
      
      //dEbug
      // if (DEBUG)
      // {	  
      // 	cout<<playerPosition[playerRollCount]<<endl;	 
      // }
      
      
      
      
      playerRollCount++;
      if(playerRollCount>=3)
	playerRollCount=0;
      
    }
    //print players and positions
    for (int i =0;i<players;i++)
    {
      cout<<"Position of player "<<i+1<<" is "<< playerPosition[i]<<"."<<endl;	 
      
    }
    // set end to false for next test case
    end = false;  
  }
   
}
