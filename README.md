# Programming Challenges - INF/UFRGS #

This repository contains the programs developed for the class of programming challenges (INF01056 - Desafios de Programação) from UFRGS

The problems are divided in weeks, each week 3 problems were assigned, an easy, a medium an a hard problem. They were all taken from [UVA Online Judge](https://uva.onlinejudge.org/index.php)

Here is the list of challenges and their definition:

**Week 1**

1. [Event Planning - 11559](http://uva.onlinejudge.org/index.php?option=com_onlinejudge&Itemid=8&page=show_problem&problem=2595)
2. [Snakes and Ladders - 11459](http://uva.onlinejudge.org/index.php?option=com_onlinejudge&Itemid=8&category=26&page=show_problem&problem=2454)
3. [Amazing - 556](http://uva.onlinejudge.org/index.php?option=com_onlinejudge&Itemid=8&page=show_problem&problem=497)

**Week 2**

1. [Add All - 10954](http://uva.onlinejudge.org/index.php?option=com_onlinejudge&Itemid=8&category=21&page=show_problem&problem=1895)
2. [Conformity - 11286](http://uva.onlinejudge.org/index.php?option=com_onlinejudge&Itemid=8&category=24&page=show_problem&problem=2261)
3. [I Can Guess The Data Structure ! - 11995](http://uva.onlinejudge.org/index.php?option=com_onlinejudge&Itemid=8&page=show_problem&problem=3146)

**Week 3**

1. [Throwing Cards Away - 10935](http://uva.onlinejudge.org/index.php?option=com_onlinejudge&Itemid=8&category=21&page=show_problem&problem=1876)
2. [Andy'Second Dictionary - 11062](http://uva.onlinejudge.org/index.php?option=com_onlinejudge&Itemid=8&category=22&page=show_problem&problem=2003)
3. [The Lonesome Cargo Distributor - 10172](http://uva.onlinejudge.org/index.php?option=com_onlinejudge&Itemid=8&category=13&page=show_problem&problem=1113)

**Week 4**

1. [Hanoi Tower Troubles Again - 10276](https://uva.onlinejudge.org/index.php?option=com_onlinejudge&Itemid=8&category=645&page=show_problem&problem=1217)
2. [Back to the 8-Queen - 11085](http://uva.onlinejudge.org/index.php?option=onlinejudge&page=show_problem&problem=2026)
3. [Expert Enough? - 1237](http://uva.onlinejudge.org/index.php?option=onlinejudge&page=show_problem&problem=3678)

**Week 5**

1. [Let Me Count The Ways - 357](http://uva.onlinejudge.org/index.php?option=com_onlinejudge&Itemid=8&category=5&page=show_problem&problem=293)
2. [Maximun Sum - 108](http://uva.onlinejudge.org/index.php?option=com_onlinejudge&Itemid=8&category=3&page=show_problem&problem=44)
3. [Dividing Coins - 562](http://uva.onlinejudge.org/index.php?option=com_onlinejudge&Itemid=8&category=7&page=show_problem&problem=503)

**Week 6**

1. [Bicoloring -10004](https://uva.onlinejudge.org/index.php?option=com_onlinejudge&Itemid=8&category=12&page=show_problem&problem=945)

2. [Beverages - 11060](https://uva.onlinejudge.org/index.php?option=com_onlinejudge&Itemid=8&category=22&page=show_problem&problem=2001)

3. [The Monocycle -10047](https://uva.onlinejudge.org/index.php?option=com_onlinejudge&Itemid=8&category=12&page=show_problem&problem=988)

**Week 7**

**Week 8**

**Week 9**