#include<stack>
#include<vector>
#include<iostream>
#include <algorithm>

using namespace std;

int main(){
  
  string testcase;
  
  while(true)
  {

    cin >>testcase;
    if (testcase == "end")
      break;
    vector<stack<int> > port;
    int stackCount=1;
    for(char& c : testcase) {
      port.push(c);
    }

    bool stackUsed=false;    
    //iterato all ships
    for (char ch = 'A' ; ch <= 'Z' ; ++ch )
    {      
      stack<int> aux;
      //empty port
      while(!port.empty())
      {
	if (port.top()==ch)
	  port.pop();
	else
	{	  
	  aux.push(port.top());
	  port.pop();
	  stackUsed=true;
	}	
      }

      // put containers back to port
      while(!aux.empty())
      {
	port.push(aux.top());
	aux.pop();
      }
      if (stackUsed)
	stackCount++;
      stackUsed=false;
    }
   
    
    cout<<stackCount<<endl; 
  } 
  
  
    

  return 0;
}
