#include<stack>
#include<iostream>
#include <algorithm>

#include <string>
using namespace std;

int main(){
  
  int casesCount,foo=1;
  


  string cases;
  getline(cin, cases);

  casesCount=atoi( cases.c_str() ); 
  while(casesCount--)
  {
    string team,judge;
    getline(cin, team);
    getline(cin, judge);
    
    
    if(team.compare(judge)==0)
      cout<<"Case "<<foo<<": Yes"<<endl;
    
    else{
      team.erase(remove_if(team.begin(), team.end(), ::isspace), team.end());
      if(team.compare(judge)==0)
	cout<<"Case "<<foo<<": Output Format Error"<<endl;
      else
	cout<<"Case "<<foo<<": Wrong Answer"<<endl;
      
    }
    foo++;
  }  
  return 0;
}
