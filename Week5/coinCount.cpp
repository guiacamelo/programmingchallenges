// explanation of the problem recursion
// http://www.algorithmist.com/index.php/Coin_Change
#include<iostream>
#include <algorithm>
#include <vector>
#include <math.h>

using namespace std;
long long count(int n, int m);
const int DEBUG=0;
const int fifty = 50;
const int quarter = 25;
const int dime = 10;
const int nickel = 5;
const int penny = 1;
int coins[]={50,25,10,5,1};
//int coins[]={1,5,10,25,50};
long long table [30001][5];




int main(){
  
   int value;



  while(cin >>value){
    if (DEBUG) cout <<value<<endl;
    // if (value < dime)
    //   cout<<"There is only 1 way to produce "<<value<<" cents change."<<endl;
    // else
    // {

    long long foo = count(value,5);
    if (foo==1)
          cout<<"There is only "<< foo <<" way to produce "<< value<<" cents change."<<endl;
    else
      cout<<"There are "<< foo <<" ways to produce "<< value<<" cents change."<<endl;
  }
  return 0;
}
long long count(int n, int m){
  
  for(int i=0;i<=n;i++){   
    for(int j=0;j<=m;j++){   
      if (i == 0)
	table[i][j] = 1;
      else if (j == 0)
	if (i%coins[j] == 0)
	  table[i][j] = 1;
	else 
	  table[i][j] = 0;
      else if (coins[j] > i)
	table[i][j] = table[i][j - 1];
      else 
	table[i][j] = table[i - coins[j]][ j] + table[i][j-1];
    }
  }
        return table[n][m];
}
