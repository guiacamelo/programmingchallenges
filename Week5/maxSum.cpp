#include<iostream>
#include <algorithm>
#include <climits>
#include <cstdio>
#include <cstring>

using namespace std;

int const SIZE =100;
int current_max =  -2147483648;

int Matrix[SIZE][SIZE] ;
int max_ = -2147483648;
int down_sum[SIZE];
  
int max_sum(int N){
  
  for (int inix=0;inix<N;inix++){
    //  memset(down_sum, 0, N * sizeof(int));
    for(int k=0;k<N;k++)
      down_sum[k]=0;
    for (int i=inix;i<N;i++){

      int local_max = 0;

      current_max =  -2147483648;    

      for (int j=0;j<N-1;j++){
	down_sum[j]=down_sum[j]+Matrix[i][j];
	
	// tHIS PREVENTS STORING NEGATIVE VALUES
	 if (local_max+down_sum[j]<0)
	   local_max = 0;
	 else
	   local_max = local_max+down_sum[j];

	
	
	current_max=max(current_max,local_max);
      }
      max_=max(current_max,max_);
    }
  }
  cout << max_<<endl;
}

int main(){
int N;  
  cin>>N;
  for (int i=0;i<N;i++){
      for (int j=0;j<N;j++){
	cin>>Matrix[i][j];
	//	cout << Matrix[i][j]<< " ";		 
			  
      }
      //      cout<<endl;
  }

  max_sum(N);
  return 0;
  
}
