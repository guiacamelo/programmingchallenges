#include<iostream>
#include <algorithm>
#include <queue> 
using namespace std;

const int DEBUG=0;

//const int N_MAX=5000;
int main()
{

  std::queue<int> deck;
  std::queue<int> empty;

// deck.push (myint);
// deck.pop();
// deck.front();
// deck.size();

  // read the values inputed, n is max card
  unsigned int n; 


  while(cin>>n)
  {
    if (n==0){
      break;
    }
    
    //    clear  queue
    std::swap( deck, empty );
    deck = queue<int>();  
    // populate queue
    for (int i=1;i<=n;i++ ){
      deck.push(i);
    }
    cout << "Discarded cards:" ;    
    while(deck.size()>=2)
    {
      
      // discarted card
      int discarted=0;
      // second card that will be moved to the end of the deck
      int movedCard=0;

      discarted = deck.front(); deck.pop();
      movedCard = deck.front(); deck.pop();
      deck.push(movedCard);
      if(deck.size()!=n-1){
            cout<<", "<<discarted;
      }else
	cout<<" "<<discarted;

    }
    cout << endl ;
    cout<<"Remaining card: "<<  deck.front() << endl ;
    
  }
}
