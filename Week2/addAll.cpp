#include<iostream>
#include <algorithm>
#include <vector> 
using namespace std;

const int DEBUG=0;

//const int N_MAX=5000;
int main()
{
  unsigned int n,countN;

  // keep intermidiate values 
  vector<int> addAll;

  // read the values inputed
  unsigned int foo; 
  
  //keeps the sum
  unsigned int sum=0, temp_sum=0; 

  while(cin>>n)
  {
    // Clearing queue
    addAll.clear();

    if (n==0)
      break;
    countN=n;
    while(countN-- > 0)
    { 
      cin >>foo;
      addAll.push_back(foo);
     
    }
    std::sort(addAll.begin(), addAll.end());       
    countN=n;
    sum = 0;
    
    while(countN-- > 1)
    {
      temp_sum = addAll[0] + addAll[1];
      addAll.erase(addAll.begin(),addAll.begin()+2);
      addAll.push_back(temp_sum);
      sum += temp_sum;
      std::sort(addAll.begin(), addAll.end());       
    }
    cout<< sum <<endl;
  }
}
