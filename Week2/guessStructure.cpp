#include<iostream>
#include<queue>
#include<stack>

using namespace std;


const int DEBUG =0;
const int PUSH =1;
const int POP =2;

int main()
{
  int n;
  
  while(cin >> n)
  {
    if(DEBUG) cout<<n<<endl;
    bool isQueue = true;
    bool isStack = true;
    bool isPQueue = true;
    
    queue<int> q;
    stack<int> s;
    priority_queue<int> pq;
    
    while(n--)
    {
      size_t operation;
      size_t value;
      
      cin  >> operation;
      cin >>value;
      
      if (DEBUG)
      {
	cout << operation <<" "<< value << endl;
      }

      if(operation ==PUSH){

	q.push(value);
	s.push(value);
	pq.push(value);
//	cout<<q.front()<<s.top()<<pq.top()<<endl;
      }else
      {

	int q_value = 0;
	if (!q.empty())
	{
	  q_value=q.front();	  
	  q.pop();
	}
	if (q_value != value)
	  isQueue = false;


	int s_value=0;


	if (!s.empty())
	{
	  s_value=s.top();	
	  s.pop();
	}
	if (s_value != value)
	  isStack = false;


	int pq_value=0;
	if(!pq.empty())
	{
	  pq_value=pq.top();
	  pq.pop();
	}
	if (pq_value != value)
	  isPQueue = false;
	
      }
    }

    if(!isQueue&&!isStack&&!isPQueue)
      cout <<"impossible"<< endl;
    else if((isQueue&&isStack) ||(isQueue&&isPQueue)||(isStack&&isPQueue))
      cout<<"not sure"<<endl;
    else if (isQueue)
      cout<<"queue"<<endl;
    else if(isStack)
      cout<<"stack"<<endl;
    else if(isPQueue)
      cout<<"priority queue"<<endl;
    
  }
  return 0;
}

