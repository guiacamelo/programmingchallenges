#include<map>
#include<vector>
#include<algorithm>
#include<iostream>

using namespace std;

int main()
{
  

  while(true)
  {
    map<vector<int>,int> classMap;
    size_t n;
    int maxPopularity = 1;
    cin >> n;
    if (n == 0)
      break;
    while(n--)
    {      
      vector<int> classes(5);
      for (int i=0;i<5;i++)
	cin >> classes[i];      
      sort(classes.begin(),classes.end());
      classMap[classes]+=1;
      int classPopularity = classMap[classes];
      maxPopularity = max(maxPopularity,classPopularity);
    }
    int sameClassesCount=0;
    int a=0;
    
    
    map<vector<int>, int>::iterator it;    
    for(it = classMap.begin(); it != classMap.end(); ++it )
    {
      if ((*it).second == maxPopularity) {
	sameClassesCount++;
	a +=(*it).second;
      }
      
      //   cout << sameClassesCount<<endl;
    }
    cout << a<<endl;
    
    
  }
  return 0;
}
