#include <iostream>
#include <cstdio>
#include <vector>
#include <cmath> 
#include <algorithm>
#include <cstring>
#include <map>

using namespace std;

const bool DEBUG = false;

const int MAX_CUTS = 55;
int cut_cost [MAX_CUTS] [MAX_CUTS];
int cuts[MAX_CUTS];

int minimum_(int left, int right){
  int mini = 999999;
  if(cut_cost[left][right] != -1) 
    return cut_cost[left][right];
  
  if(left + 1 == right) 
    return cut_cost[left][right] = 0;
  
  
  for(int i = left + 1; i < right; i++) {
    mini = min(mini, minimum_(left, i) + minimum_(i, right) + cuts[right] - cuts[left]);
  }
  return cut_cost[left][right] = mini;
  
}



int main(){
  

  int lenght;

  while(cin>>lenght){
    if (lenght==0)
      break;

    if (DEBUG) cout<<lenght<<endl;
    
    int n,n_aux;
    int count=1;
    cin>>n;  if (DEBUG) cout<<n<<endl;
    n_aux=n;
    
    while(n_aux--){
      int newcut;
      cin>>newcut; if (DEBUG) cout<<newcut<<endl;
      cuts[count++]=newcut;
    }
    int minimum_cutting=0;
    

    cuts[0]=0;cuts[n+1]=lenght;
    memset(cut_cost, -1, sizeof cut_cost);

    minimum_cutting = minimum_(0,n+1);
    cout<<"The minimum cutting is "<< minimum_cutting <<"."<<endl;
  }
  return 0;
}
