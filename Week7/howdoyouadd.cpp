

#include <iostream>
#include <cstdio>
#include <vector>
#include <cmath>
#include <algorithm>
#include <cstring>
#include <map>

using namespace std;

const bool DEBUG = true;
const int SIZE = 102;
const long long MOD = 1000000LL;
static long long Matrix[SIZE][SIZE];

int N,K;

long long count_ways(int n, int k) {
  long long ways = 0;
  
  if(n == 0 ) 
    return 1;
  
  if (k == 1)
    return 1;
  
  
  
  if(Matrix[n][k] >= 0) 
    return Matrix[n][k];
  for(int i = 0; i <= n; ++i){

    ways = (count_ways(n - i, k - 1) + ways) ;   
    if (DEBUG) cout << ways<<endl;
    ways = ways % MODif (DEBUG) cout << ways <<endl;
  }

  Matrix[n][k] = ways;

  return ways;
}



int main(){

  memset(Matrix, -1, sizeof(Matrix));
  long long ways=0;

  while((cin>>N)&&(cin>>K)){
    if((N == 0)&(K==0)){
      break;
    }
    if (DEBUG) cout << N << " " << K<<endl;
    ways = count_ways(N,K);
    cout<<ways<<endl;
  }
  
  return 0;
}
