

#include<stdio.h>
#include<iostream>
#include<string.h>
#include<iostream>
#include <algorithm>
#include <vector>
#include <string>
#include <cstring>

#define SIZE 1000
using namespace std;

static char A[SIZE], B[SIZE];
static int Matrix[SIZE][SIZE];


int max_sub(int sizeA,int sizeB){
  for(int i = 1; i <= sizeA; ++i){
    for(int j = 1; j <= sizeB; ++j){
      if(A[i-1] == B[j-1])
	Matrix[i][j] = Matrix[i-1][j-1] + 1;
      else
	Matrix[i][j] = max(Matrix[i-1][j], Matrix[i][j-1]);
    }
  }
  return Matrix[sizeA][sizeB];

}
 
int main(){

while(cin.getline(A,SIZE)&&cin.getline(B,SIZE))
{

 
    int sizeA = strlen(A);
    int sizeB = strlen(B);
    
    
    cout<<max_sub(sizeA,sizeB)<<endl;
 
  }
  return 0;
}
