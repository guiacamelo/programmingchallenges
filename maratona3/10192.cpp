#include <iostream>
#include <vector>
#include <sstream>
#include <algorithm>
#include <iterator>
#include <cstring>
#include <cmath>
#include <queue>
#include <stack>
#include <map>
#include <set>
#include <iomanip>

using namespace std;

#define MAX_CITIES 101

int citiesMatrix[MAX_CITIES][MAX_CITIES];

int main(){
	
	
	int numCase = 1;
	string sequence1, sequence2;
	while(getline(cin, sequence1)) {

		if(sequence1 == "#")
			break;

		getline(cin, sequence2);
		memset(citiesMatrix, 0, sizeof(citiesMatrix));

		for(int i = 1; i < sequence1.size() + 1; i++) {
			for(int j = 1; j < sequence2.size() + 1; j++) {
				if(sequence1[i - 1] == sequence2[j - 1]){
					citiesMatrix[i][j] = citiesMatrix[i - 1][j - 1] + 1;
				} else {
					citiesMatrix[i][j] = max(citiesMatrix[i - 1][j], citiesMatrix[i][j - 1]);
				}
			}
		}
		
		cout << "Case #" << numCase << ": you can visit at most " << 
			citiesMatrix[sequence1.size()][sequence2.size()] << " cities." << endl; 
		numCase++;
	}
}