#include <iostream>
#include <string>
#include <sstream>
#include <algorithm>

using namespace std;

long inverte( long numero) {
    std::ostringstream ss;
    ss << numero;
    string number = ss.str();  
    reverse(number.begin(), number.end());
    return atol(number.c_str());
}


int main() {
    
    int N;
    int rodadas;
    long numOrigem;
    long numInvertido;
    cin >> N;
    
    while(N--){
        cin >> numOrigem;
        
        for(rodadas = 1; rodadas <= 1000; rodadas++ ) {   
                 
            numInvertido = inverte(numOrigem);
            
            numOrigem = numOrigem + numInvertido;
                        
            if(numOrigem == inverte(numOrigem)) {
                cout << rodadas << " " << numOrigem << endl;
                break;
            }
        }
    }
    
    return 0;
}

