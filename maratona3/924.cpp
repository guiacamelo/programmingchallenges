#include <iostream>
#include <algorithm>
#include <vector>
#include <queue>

using namespace std;

typedef pair<int, int> ii;
typedef vector<int> vi;
typedef vector<ii> vii; 

vector<vii> AdjList;
vi p;

int maxBoom;
int firstBoom;
int cont;

 
int main() {
     
    int E, N, T, source;

    cin >> E;
    
    AdjList.assign(E, vii());
    
    for (int empregado = 0; empregado < E; empregado++) {
        cin >> N;
        for (int numAmigos = 0; numAmigos < N; numAmigos++) {
            int amigo;
            cin >> amigo;
            AdjList[empregado].push_back(ii(amigo, 0));
        }
    }
    
    cin >> T;
    while(T--) {
        maxBoom = -1;
        firstBoom = -1;
        cont = 0;

        cin >> source;
        vi dist(E, 1000000000); dist[source] = 0;
        queue<int> q; q.push(source);  
        p.assign(E, -1);
        int layer = -1;                            
        int aux = 0;     
        
        while (!q.empty()) {
            int u = q.front(); q.pop();
            if (dist[u] != layer) {
                if (dist[aux] != 0 && maxBoom < cont) {
                    maxBoom = cont;
                    firstBoom = dist[aux];
                }
                cont = 0;
            }

            aux = u;
            layer = dist[u];
            cont++;
            for (int j = 0; j < (int)AdjList[u].size(); j++) {
                ii v = AdjList[u][j];                           
                if (dist[v.first] == 1000000000) {
                    dist[v.first] = dist[u] + 1;                 
                    p[v.first] = u;
                    q.push(v.first);
                }
            }
        }
        
        if (maxBoom < cont) {
            maxBoom = cont;
            firstBoom = dist[aux];
        }

        if (firstBoom > 0) {
            cout << maxBoom << " " << firstBoom << endl;
        } else {
            cout << firstBoom << endl;
        }       
    }
     
    return 0;
}
