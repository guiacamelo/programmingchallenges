#include <iostream>
#include <vector>
#include <sstream>
#include <algorithm>
#include <iterator>
#include <cstring>
#include <cmath>
#include <queue>
#include <stack>
#include <map>
#include <set>
#include <iomanip>
#include <utility>

using namespace std;

int letters[26];

typedef pair <int, char> ic;

typedef bool (*comp)(ic, ic);
bool compare(ic a, ic b)
{
	if(a.first == b.first) {
		if(a.second > b.second) return true;
		return false;
	}
	if(a.first < b.first) return true;
	return false;
}

int main(){

	memset(letters, 0, sizeof(letters));

	int lines;
	cin >> lines;

	string line;
	cin.ignore();
	while(lines--) {
		getline(cin, line);
		// cout << line << endl;

		for (int i = 0; i < line.size(); ++i) {
			char letter = line[i];
			if (letter >= 'a' && letter <= 'z'){
				letters[letter - 'a']++;
			} else if (letter >= 'A' && letter <= 'Z') {
				letters[letter - 'A']++;
			}
		}
	}

	priority_queue < ic, vector < ic >, comp > icList(compare);

	for (int i = 25; i >= 0; --i) {
		if(letters[i] > 0){
			icList.push(ic(letters[i], 'A' + i));
		}
	}

	while(!icList.empty()) {
		ic top = icList.top(); icList.pop();
		cout << top.second << " " << top.first << endl;
	}
}