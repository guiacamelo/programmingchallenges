#include<iostream>
#include <vector>
#include <stdio.h>
#include <algorithm>
#include <queue>
#include <iomanip>
using namespace std;

const bool DEBUG = false;
const int SIZE = 21;

vector<int>country_graph[SIZE];

int bfs(int source, int destination)
{
    queue< int >q;
    q.push(source);
    
    int visited[100]={0};
    int path_to_dest[100];

    visited[source]=1;
    path_to_dest[source]=0;

    while(!q.empty())
    {
        int u=q.front();
        for(int l=0; l<country_graph[u].size(); l++)
        {
            int v = country_graph[u][l];
            if(!visited[v])
            {
	      visited[v]=1;
	      path_to_dest[v] = path_to_dest[u]+1;
	      q.push(v);
	      if(DEBUG) cout<<v<<endl;
            }
        }
        q.pop();
    }
    return path_to_dest[destination];

}

int main()
{
  int foo, bar;
  int testcase=1;
  
  while(cin>>foo)
  {
    if(DEBUG) cout<<foo;
    //read first line
    for(int j=0; j<foo; j++)
    {
      cin>>bar;
      
      //populte the country_country_graph
      country_graph[1].push_back(bar);
      country_graph[bar].push_back(1);
      
      if(DEBUG) cout<<" "<<bar;
    }
    
    if(DEBUG) cout<<endl;
    //finish first line
    
    // read next 19 lines
    for(int i=2; i<=19; i++)
    {
      cin >> foo ;
      if(DEBUG) cout<<foo;
      
      
      for(int j=0; j<foo; j++)
      {
	cin >> bar ;
	
	if(DEBUG) cout<<" "<<bar;
	
	country_graph[i].push_back(bar);
	country_graph[bar].push_back(i);
      }
      if(DEBUG) cout<<endl;
    }
    
    
    int source, destination, country_pairs;
    cin >> country_pairs;
    
    cout<<"Test Set #"<<testcase++<<endl;
    for(int i=0; i<country_pairs; i++)
    {
      cin >> source;
      cin >> destination;
      cout<<setw(2)<<std::setfill(' ')<<source<<" to "<<setw(2)<<std::setfill(' ')<<destination<<": "<< bfs(source,destination)<<endl;
    }
    cout<<endl;
    //CLear country_country_graph
    for(int i=0; i<SIZE; i++)
    {      
      country_graph[i].clear();
    }
  }
  return 0;
}

