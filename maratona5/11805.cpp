#include <iostream>
#include <vector>
#include <sstream>
#include <algorithm>
#include <iterator>
#include <cstring>
#include <cmath>
#include <queue>
#include <stack>
#include <map>
#include <set>
#include <iomanip>
#include<stdio.h>

using namespace std;
const int DEBUG =0;

int main() {
  int T;
  cin >> T;
  int ans;
  int cases=0;
  while(T--)
  {
    cases++;
    int N,K,P;
    cin >>N;
    cin >>K;
    cin >>P;
    ans = ((K-1+P)%N)+1;
    if(DEBUG)  cout << ans<<endl;
    
    
    cout << "Case " << cases << ": " << ans << endl;
    
  }
}
