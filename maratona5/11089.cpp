#include <iostream>
#include <vector>
#include <sstream>
#include <algorithm>
#include <iterator>
#include <cstring>
#include <cmath>
#include <queue>
#include <stack>
#include <map>
#include <set>
#include <iomanip>
#include<stdio.h>

using namespace std;
const int DEBUG =0;
const int MAX =45;

int main() {
  int test,n;
  int Fi[MAX] = {1,2};
  for(int i = 2; i < 45; i++){
   //Calc Fibonnaci
   Fi[i] = Fi[i-1] + Fi[i-2];
   if(DEBUG)  cout << Fi[i] <<endl;
  }
  cin>>test;
  int j;
  while(test--) {
    cin>>n;
    j = MAX-1;
    while(Fi[j] > n)	
      j--;
    for(; j >= 0; j--) {
      if(Fi[j] <= n){
	n = n - Fi[j];
	cout<<"1";
      }
      else
        cout<<"0";
    }
    cout<<endl;
  }
  return 0;
}
